https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#actuator.endpoints

Even secured, basic info is still exposed.
http://localhost:8001/actuator/health
http://localhost:8001/actuator/info


All endpoints are Enabled by default, but not exposed.
Exposed via JMX or HTTP(disabled for security purposes).


Mainly used
    health Endpoints 
          - docker swarm or kubernetes call your health
    Metrics Endpoints 
          - Live on what happening with your application (memory , performance, insights)
          - Grafana , New Relic , Data Dog
    Loggers Endpoint 
          - All dev teams wishes to enable diagnostic loggers for issue finding for some-time.


New Relic , Data Dog  - SAS (push model)
Prometheus and Grafana - Self Managed (pull model)