package com.sara.config;

import com.sara.controller.AccountController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration(proxyBeanMethods = false)
//Ideally your actuator endpoints will be protected with roles.
public class ActuatorSecurityConfig extends WebSecurityConfigurerAdapter{

    final Logger logger = LoggerFactory.getLogger(ActuatorSecurityConfig.class);

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        logger.info("Actuator Info Logger on SecurityConfig");
        http
        .requestMatcher(EndpointRequest.toAnyEndpoint());
        //.authorizeRequests((req) -> req.anyRequest().hasRole("ADMIN"));
        http.httpBasic();
    }
}
