package com.sara.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.sara.repository.AccountRepository;
import com.sara.model.Account;
import java.util.List;
import java.util.ArrayList;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public List<Account> findAll() {

        var it = accountRepository.findAll();

        var accounts = new ArrayList<Account>();
        it.forEach(e -> accounts.add(e));

        return accounts;
    }

    public Long count() {

        return accountRepository.count();
    }

    public void deleteById(Long userId) {

        accountRepository.deleteById(userId);
    }
}
