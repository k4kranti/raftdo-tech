package com.sara;

import com.sara.config.ActuatorSecurityConfig;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HealthApplication {
    final Logger logger = LoggerFactory.getLogger(HealthApplication.class);


    public static void main(String args[]){
        new SpringApplicationBuilder(HealthApplication.class).run(args);
    }

    @Bean
    MeterRegistryCustomizer<MeterRegistry> configurer(@Value("${spring.application.name}") String applicationName) {
        logger.info("Application is MeterRegistered");
        return (registry -> registry.config().commonTags("application", applicationName));
    }
    
}
