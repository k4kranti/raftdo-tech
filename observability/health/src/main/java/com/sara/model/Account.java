package com.sara.model;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Entity
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String accountName;
    private String accountType;
    private String balance;

    public Account() {}

    public Account(String accountName, String accountType, String balance) {
        this.accountName = accountName;
        this.accountType = accountType;
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(accountName, account.accountName) &&
                Objects.equals(accountType, account.accountType) &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountName, accountType, balance);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Account{");
        sb.append("id=").append(id);
        sb.append(", accountName='").append(accountName).append('\'');
        sb.append(", accountType='").append(accountType).append('\'');
        sb.append(", balance='").append(balance).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
