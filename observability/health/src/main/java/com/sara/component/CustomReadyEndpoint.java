package com.sara.component;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id="ready")
// Your micro app is not ready for traffic in containers, need to know the health of endpoint.
public class CustomReadyEndpoint {
    private String ready = "APP_NOT_READY";

    @ReadOperation
    public String getReady(){
        return ready;
    }
    @EventListener(ApplicationReadyEvent.class)
    public void setReady(){
         ready = "READY";
    }
}
