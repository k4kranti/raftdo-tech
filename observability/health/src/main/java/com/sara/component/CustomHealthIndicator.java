package com.sara.component;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
// You have legacy third party API, need to know the health of that service.
@Component("custom-api")
public class CustomHealthIndicator implements HealthIndicator{
    
    @Override
    public Health health() {
        //perform custom health check - say a dynamic rates third party API
        double value = ThreadLocalRandom.current().nextDouble();
        //status is healthy
        Health.Builder status = Health.up();
         //status is unhealthy
        if (value > 0.9) {
            status = Health.down()
                        .withDetail("response_code","500")
                        .withException(new Throwable());
        }
        //map additional attributes
        Map<String, Object> legacyApp = new HashMap<>();
        legacyApp.put("application", "legacy");
        legacyApp.put("dynamic_rates", "random_calls");
        legacyApp.put("response_ms","100");
        status.withDetails(legacyApp);

        return status.build();
    }
}
