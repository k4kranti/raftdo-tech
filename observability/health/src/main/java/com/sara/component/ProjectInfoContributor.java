package com.sara.component;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.actuate.info.*;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;

@Component
public class ProjectInfoContributor implements InfoContributor{
    
    @Override
    public void contribute(Info.Builder builder){
        //add your service api project info
        Map<String, String> details = new HashMap<>();
        details.put("service_name", "accounts");
        details.put("project_team","Bluesprint team");
        details.put("project_contact","accounts_support@raftdo.com");
        Map<String, String> details1 = new HashMap<>();
        details1.put("service_name", "rates");
        details1.put("project_team","Scrum&Coke team");
        details1.put("project_contact","rates_support@raftdo.com");

        builder.withDetail("accounts",details)
                .withDetail("rates",details1);
    }
}

