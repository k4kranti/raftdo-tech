package com.sara.controller;

import com.sara.model.Account;
import com.sara.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {

    final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @GetMapping("/accounts")
    public List<Account> allUsers() {
        logger.info("Actuator Info Logger on allUsers");
        return accountService.findAll();
    }

    @GetMapping("/accounts/count")
    public Long count() {

        return accountService.count();
    }

    @DeleteMapping("/accounts/{id}")
    public void delete(@PathVariable String id) {

        Long accountId = Long.parseLong(id);
        accountService.deleteById(accountId);
    }
}
    
