*************** OpenSSL to create self-signed certificates ************* 
    Install openssl  
> brew install openssl 
    check which version of openssl in your system command line. 
> openssl version 
    ~ output: LibreSSL 2.8.3 

> mkdir openssl 
> cd openssl 
    create the file 
> touch gen.dev.sh 
> change mod the file access 
> chmod +x gen.dev.sh 
    open in your fav editor - like visual source code  
> code . 

*************** STEP - Generate CA's private key and self-signed certificate ************* 
    X.509 certificates are used in many Internet protocols, including TLS/SSL, which is the basis for HTTPS, the secure protocol for browsing the web. syntax ref https://man.openbsd.org/openssl.1#req 
    RSA is a public-key cryptosystem that is widely used for secure data transmission 
    - req tells x509 output certificate with rsa 4096 bytes 
    -keyout : to write private key  
    -out : to write certificate 
    -nodes : private key is not ENCRYPTED 

> openssl req -x509 -newkey rsa:4096 -days 365 -nodes -keyout ca-key.pem -out ca-cert.pem

    It will start generating the private key 
    Asks for PEM pass phrase: (you need this in debugging) 
    encrypted private key and base64 encoded certificate is generated. 
    Congratulations! You should now have two files: ca-key.pem (your private key) and ca-cert.pem (your root certificate). 

> ls -ltr 
    ~ On windows you need some additional utilities like winpty openssl  

CA's self-signed certificate is public and its base64 encoded, we can see using x509 
    -in : your generated certificate 
    -noout : don't output original encoded value  
    -text : human readable format 

> openssl x509 -in ca-cert.pem -noout -text 

    we can see Issuer: and Subject: are same as its self-signed certificate 
    output sample (by default Validity is one month) 
    C = Countrycode, ST = State, O = Organization, CN = yourdomain.com 
    Serial Number: 10124412140147456313 (0x8c81233335243939) 
        Signature Algorithm: sha256WithRSAEncryption 
        Issuer: C=USA, ST=NC, L=Charlotte, O=RAFTDO LLC, OU=Tech, CN=*.raftdo.com/emailAddress=k4kranti@gmail.com 
        Validity 
            Not Before: Jan 24 01:47:15 2022 GMT 
            Not After : Jan 24 01:47:15 2023 GMT 
        Subject: C=USA, ST=NC, L=Charlotte, O=RAFTDO LLC, OU=Tech, CN=*.raftdo.com/emailAddress=k4kranti@gmail.com 

**********    Generate server's private key and certificate signing request (CSR) **************** 
> openssl req  -newkey rsa:4096 -nodes -keyout ./certs/server-key.pem -out ./certs/server-req.pem -subj "/C=USA/ST=NC/L=Charlotte/O=RAFTDO LLC/OU=Servers/CN=*.raftdo.com/emailAddress=admin_service@raftdo.com" 
    -newkey : Create a new certificate request and a new private key 
    -keyout : The file to write the newly created private key to. 
    -nodes : Do not encrypt (dev) the private key. 

**************** Generate Subject Alternative Name ************************ 
    Subject Alternative Name is an extension to X.509 that allows various values to be associated with a security certificate 
    Add all your local domains or virtual ips for easy local testing 
> touch server-ext.cnf 
> chmod +x server-ext.cnf  
> echo 'subjectAltName=DNS:*.raftdo.com,IP:192.168.106.99,IP:127.0.0.1,IP:0.0.0.0' > server-ext.cnf  

********** Use CA's private key to sign server's CSR and get back signed certificate********** 
> openssl x509 -req -days 60 -in ./certs/server-req.pem -CA ./certs/ca-cert.pem -CAkey ./certs/ca-key.pem -CAcreateserial -out ./certs/server-cert.pem -extfile server-ext.cnf 


*************** Update your certificate chain **************************** 
     In Mac, via CLI keychain   
> sudo security add-trusted-cert -d -r trustRoot -k "/Library/Keychains/System.keychain" "./certs/dev/ca-cert.pem" 
    In Mac, via  open keychain app, import under system and say always trust. 


****** Other encryptions to try via genpkey *** ********** 
> openssl genpkey -algorithm RSA -out ca-key.pem -aes-256-cbc 

    openssl genpkey utility has superseded the genrsa utility. 
    allows you to generate RSA, RSA-PSS, EC, X25519, X448, ED25519 and ED448. 
    cipher used to encrypt key -aes-256-cbc 

