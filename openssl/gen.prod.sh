mkdir -p certs
rm ./certs/*.pem
rm .srl
# 1. Generate CA's private key and self-signed root certificate 
openssl req -x509 -newkey rsa:4096 -days 365 -keyout ./certs/ca-key.pem -out ./certs/ca-cert.pem -subj "/C=US/ST=NC/L=Charlotte/O=RAFTDO LLC/OU=Tech/CN=*.raftdo.com/emailAddress=k4kranti@gmail.com"

echo "CA's self-signed certificate"
openssl x509 -in ./certs/ca-cert.pem -noout -text

# 2. Generate server's private key and certificate signing request (CSR)
openssl req  -newkey rsa:4096 -nodes -keyout ./certs/server-key.pem -out ./certs/server-req.pem -subj "/C=US/ST=NC/L=Charlotte/O=RAFTDO LLC/OU=Servers/CN=*.raftdo.com/emailAddress=k4kranti@gmail.com"

# 3. Use CA's private key to sign server's CSR and get back signed certificate 
openssl x509 -req -days 365 -in ./certs/server-req.pem -CA ./certs/ca-cert.pem -CAkey ./certs/ca-key.pem -CAcreateserial -out ./certs/server-cert.pem -extfile server-ext.cnf

echo "CA's server signed certificate"
openssl x509 -in ./certs/server-cert.pem -noout -text

echo "Exporting certificates to project..."
cp ./certs/*.pem ../vault/certs/prod