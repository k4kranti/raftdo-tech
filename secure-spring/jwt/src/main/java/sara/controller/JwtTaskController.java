package sara.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;
import sara.model.JwtTask;
import sara.service.JwtTaskService;

import java.util.List;

@RestController
@RequestMapping(path = "/tasks")
public class JwtTaskController {

    private static final Log logger = LogFactory.getLog(JwtTaskController.class);
    JwtTaskService taskService;

    public JwtTaskController(JwtTaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = "application/json")
    public List<JwtTask> getTasks(){
        return taskService.getTasks();
    }

    @PostMapping(consumes ="application/json")
    public JwtTask postTasks(@RequestBody JwtTask task){
        logger.info("We are in POST Call" + task);
        return taskService.postTasks(task);
    }
}
