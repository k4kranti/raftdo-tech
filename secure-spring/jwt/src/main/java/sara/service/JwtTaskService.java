package sara.service;

import org.springframework.stereotype.Service;
import sara.model.JwtTask;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtTaskService {

    List<JwtTask> taskList = new ArrayList<>();
    JwtTask task1 = new JwtTask(1, "STEPHEN" , "PROJECT PLAN", true);
    JwtTask task2 = new JwtTask(2, "SARA LEE", "TEAM ENGAGEMENT", false);

    public JwtTaskService(){
        taskList.add(task1);
        taskList.add(task2);
    }

    public List<JwtTask> getTasks(){
        return taskList;
    }

    public JwtTask postTasks(JwtTask task){
        taskList.add(task);
        return task;
    }
}
