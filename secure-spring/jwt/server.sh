#!bin/bash
echo "docker"
docker start CONTAINER raftdo
docker run -p 9990:9990 --name authserver -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e DB_VENDOR=h2 quay.io/keycloak/keycloak:16.1.1
#Database info: {databaseUrl=jdbc:h2:/opt/jboss/keycloak/standalone/data/keycloak, databaseUser=SA, databaseProduct=H2 1.4.197 (2018-03-18), databaseDriver=H2 JDBC Driver 1.4.197 (2018-03-18)

JWT=$(curl -d 'client_id=jwt' -d 'username=user' -d 'password=password' -d 'grant_type=password' -d 'client_secret=Pc0zp0b7KCvZDuIlP1VAJOSYJ7jy64sW'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq .access_token)


