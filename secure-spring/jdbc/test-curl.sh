#!bin/bash
echo "unauthorized tasks:"
curl localhost:8080/tasks -v | jq '.error'
echo "authorized tasks with READ/WRITE role:"
curl -u user:password localhost:8080/tasks -v | jq
echo "Adding task by authorized WRITE role"
curl -u user:password -d '{"id": 10,"owner": "ROBERT","task": "QA","completed": false}' -H "Content-Type: application/json" -X POST http://localhost:8080/tasks | jq
echo "Adding task by authorized READ role"
curl -u tester:password -d '{"id": 11,"owner": "HELEN","task": "AUTOMATION","completed": false}' -H "Content-Type: application/json" -X POST http://localhost:8080/tasks | jq
