package com.sara.repository;

import com.sara.model.JTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JTaskRepository extends JpaRepository<JTask, Long> {
}
