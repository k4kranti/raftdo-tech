package com.sara.service;

import com.sara.model.JTask;
import com.sara.repository.JTaskRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JTaskService {

    JTaskRepository repository;

    public JTaskService(JTaskRepository repository) {
        this.repository = repository;
    }

    public List<JTask> getTasks(){
        return repository.findAll();
    }

    public JTask postTasks(JTask task){
        return repository.save(task);
    }
}
