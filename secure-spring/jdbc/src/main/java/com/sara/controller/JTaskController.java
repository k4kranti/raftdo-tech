package com.sara.controller;

import com.sara.model.JTask;
import com.sara.service.JTaskService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/tasks")
public class JTaskController {

    private static final Log logger = LogFactory.getLog(JTaskController.class);

    JTaskService taskService;

    public JTaskController(JTaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = "application/json")
    public List<JTask> getTasks(){
        return taskService.getTasks();
    }

    @PostMapping(consumes ="application/json")
    public JTask postTasks(@RequestBody JTask task){
        logger.info("We are in POST Call" + task);
        return taskService.postTasks(task);
    }
}
