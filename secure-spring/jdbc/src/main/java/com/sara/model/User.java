package com.sara.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * This is spring security specific user table with column names - don't change domain model
 */
@Entity(name="users")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User implements java.io.Serializable{
    @Id
    UUID id;
    @Column(name="username" , unique = true)
    String username;
    String password;
    Boolean enabled = true;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    List<UserAuthority> authorities = new ArrayList<>();

    public List<UserAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<UserAuthority> authorities) {
        this.authorities = authorities;
    }

    public void addAuthority(String authority){
        authorities.add(new UserAuthority(this,authority));
    }

    public User(){
    }
    public User(UUID id, String username, String password, Boolean enabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }
}
