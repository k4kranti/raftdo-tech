package com.sara.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

/**
 * This is spring security specific authorities table with column names - don't change domain model
 */

@Entity(name="authorities")
@Getter
@Setter
public class UserAuthority implements java.io.Serializable{

    @Id
    UUID id;
    @ManyToOne
    @JoinColumn(name="username",referencedColumnName = "username")
    User user;
    String authority;

    public UserAuthority(User user, String authority){
        this.id = UUID.randomUUID();
        this.user = user;
        this.authority= authority;
    }

    public UserAuthority() {}

}
