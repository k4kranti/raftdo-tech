package com.sara.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import javax.sql.DataSource;

@EnableWebSecurity
@Configuration
public class JdbcWebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Log logger = LogFactory.getLog(JdbcWebSecurityConfig.class);
    @Value("${security.csrf-enable}")
    private boolean csrfEnabled;

    @Autowired
    DataSource dataSource;

    // http config for authorize requests
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests( a -> a
                        .antMatchers("/h2-console/**")
                        .permitAll()
                        .mvcMatchers(HttpMethod.GET,"/tasks","tasks/**").hasAuthority("READ")
                        .anyRequest().hasAuthority("WRITE")
                ).httpBasic();
        // fix H2 database console: Refused to display ' in a frame because it set 'X-Frame-Options' to 'deny'
        http.headers().frameOptions().sameOrigin();
        logger.info("csrfEnabled :" + csrfEnabled);
        if (!csrfEnabled) {
            http.csrf()
                    .ignoringAntMatchers("/h2-console/**")
                    .disable();
        }
    }

    @Bean
    UserDetailsService userDetailsService(DataSource dataSource){
        return new JdbcUserDetailsManager(dataSource);
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * If there is initialization to table.
     @Autowired
     public void configure(AuthenticationManagerBuilder auth) throws Exception {
     auth.jdbcAuthentication()
     .dataSource(dataSource)
     .withDefaultSchema()
     .withUser(User.withUsername("user")
     .password(passwordEncoder.encode("password"))
     .roles("USER"));
     } **/

    /**
     * If there is no Authority table created then add these lines.
     @Bean
     UserDetailsService userDetailsService(DataSource dataSource){
     return new JdbcUserDetailsManager(dataSource){
     @Override
     protected List<GrantedAuthority> loadUserAuthorities(String username){
     return AuthorityUtils.createAuthorityList("ROLE_USER");
     }
     };
     }
     **/
}
