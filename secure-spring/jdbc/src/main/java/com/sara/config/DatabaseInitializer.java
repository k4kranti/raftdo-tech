package com.sara.config;

import com.sara.model.JTask;
import com.sara.model.User;
import com.sara.repository.JTaskRepository;
import com.sara.repository.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;

@Configuration
public class DatabaseInitializer implements SmartInitializingSingleton {

    private static final Log logger = LogFactory.getLog(DatabaseInitializer.class);

    private final JTaskRepository taskRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public DatabaseInitializer(JTaskRepository taskRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void afterSingletonsInstantiated() {

        logger.info("DatabaseInitializer ..");

        this.taskRepository.save(new JTask(1, "STEPHEN" , "PROJECT PLAN", true));
        this.taskRepository.save(new JTask(2, "SARA LEE", "TEAM ENGAGEMENT", false));
        this.taskRepository.save(new JTask( 3, "WALT" ,"ORDER DELIVERY SERVICE", false));
        this.taskRepository.save(new JTask( 4, "ALEX WATER" , "HYBRID WORK PLAN", true));

        User user = new User(UUID.randomUUID(),"user", this.passwordEncoder.encode("password"),true);
        user.addAuthority("READ");
        user.addAuthority("WRITE");

        User tester = new User(UUID.randomUUID(),"tester", this.passwordEncoder.encode("password"),true);
        tester.addAuthority("READ");

        userRepository.save(user);
        userRepository.save(tester);

        Iterable<User> users = userRepository.findAll();
        for (User login: users) {
            logger.info("Username :" + login.getUsername());
            logger.info("getAuthorities :" + login.getAuthorities());

        }

    }
}
