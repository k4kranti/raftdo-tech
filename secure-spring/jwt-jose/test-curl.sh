#!bin/bash
echo "__________________"
echo "<<< get the user token"
USER_TOKEN=$( curl -d 'client_id=jose' -d 'username=user' -d 'password=password' -d 'grant_type=password' -d 'client_secret=AiH0odzyQzo4xgzFPg74t9N4Qc7Ulyft'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq -r '.access_token')
echo $USER_TOKEN
echo "<<<< get the tester token"
TESTER_TOKEN=$( curl -d 'client_id=jose' -d 'username=tester' -d 'password=password' -d 'grant_type=password' -d 'client_secret=AiH0odzyQzo4xgzFPg74t9N4Qc7Ulyft'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq -r '.access_token' )
echo $TESTER_TOKEN
echo "<<<< get the admin token"
ADMIN_TOKEN=$( curl -d 'client_id=jose' -d 'username=admin' -d 'password=admin' -d 'grant_type=password' -d 'client_secret=AiH0odzyQzo4xgzFPg74t9N4Qc7Ulyft'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq -r '.access_token' )
echo $ADMIN_TOKEN

echo "________user access his tasks_____________"
curl -H "Authorization: Bearer ${USER_TOKEN}" localhost:8083/tasks  | jq

echo "--------tester access his tasks___________"
curl -H "Authorization: Bearer ${TESTER_TOKEN}" localhost:8083/tasks | jq

echo "-----Using tester auth_token and accessing user data 403 status Unauthorized----"
curl -H "Authorization: Bearer ${TESTER_TOKEN}" localhost:8083/tasks/1 -v | jq

echo "--------user try update status___________"
curl -H "Authorization: Bearer ${USER_TOKEN}" -X PUT localhost:8083/tasks/1/completed -v | jq

echo "--------tester (READ SCOPE) try update status _______WWW-Authenticate: Bearer error=insufficient_scope ---"
curl -H "Authorization: Bearer ${TESTER_TOKEN}" -X PUT localhost:8083/tasks/2/completed -v | jq

echo "--------user try update record___________"
curl -H "Authorization: Bearer ${USER_TOKEN}" -H "Content-Type: text/plain"  -d "MICHEAL PHELPS" -X PUT localhost:8083/tasks/1/update  -v | jq