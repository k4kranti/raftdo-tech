package sara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import sara.model.JoseTask;

import java.util.Optional;
import java.util.function.BiFunction;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class JoseTaskApplication {
    public static void main(String[] args) {
        SpringApplication.run(JoseTaskApplication.class, args);
    }

    //define spel function for easy and clean access
    @Bean
    BiFunction<Optional<JoseTask>, String, Boolean> owner(){
        return ((joseTask, owner) -> joseTask
                .filter(task -> task.getOwner().equalsIgnoreCase(owner))
                .isPresent());
    }
}


