package sara.service;

import org.springframework.stereotype.Service;
import sara.model.JoseTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JoseTaskService {

    List<JoseTask> taskList = new ArrayList<>();
    JoseTask task1 = new JoseTask(1, "user" , "PROJECT PLAN", false);
    JoseTask task2 = new JoseTask(2, "tester", "TEST ENGAGEMENT", false);

    public JoseTaskService(){
        taskList.add(task1);
        taskList.add(task2);
    }

    public List<JoseTask> getTasks(String owner){
        return taskList;
    }

    public List<JoseTask> getTasksByOwner(String owner){
        List<JoseTask> ownerTasks = new ArrayList<>();
        for (JoseTask task : taskList) {
            if(task.getOwner().equalsIgnoreCase(owner)){
                ownerTasks.add(task);
            }
        }
        return ownerTasks;
    }

    public Optional<JoseTask> read(long id){
        Optional<JoseTask> taskOptional = null;
        for (JoseTask task : taskList) {
           if(task.getId() == id){
               taskOptional = Optional.of(task);
           }
        } return taskOptional;
    }

    public JoseTask save(JoseTask task){
        taskList.add(task);
        return task;
    }

    public void updateTask(long id,String text){
        for (JoseTask task : taskList) {
            if(task.getId() == id){
                task.setTask(text);
            }
        }
    }

    public void completeTask(long id){
        for (JoseTask task : taskList) {
            if(task.getId() == id){
                task.setCompleted(true);
            }
        }
    }
}
