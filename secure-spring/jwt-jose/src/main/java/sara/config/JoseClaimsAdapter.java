package sara.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.MappedJwtClaimSetConverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JoseClaimsAdapter implements Converter<Map<String, Object>, Map<String, Object>> {

    private static final Log logger = LogFactory.getLog(JoseClaimsAdapter.class);

    private final MappedJwtClaimSetConverter delegate = MappedJwtClaimSetConverter.withDefaults(Collections.emptyMap());

    public Map<String, Object> convert(Map<String, Object> claims) {
        Map<String, Object> convertedClaims = this.delegate.convert(claims);

        String username = (String) convertedClaims.get("username");
        logger.debug("claim username :" + username);
        convertedClaims.put("username", username);

        logger.debug("********* realm_access claims *******");
        Map<String, List<String>> realmAccess = (Map<String, List<String>>) convertedClaims.get("realm_access");
        convertedClaims.put("realm_roles",realmAccess.values());

        logger.debug("********* resource_access claims *******");
        Map<String, List<Object>> resourceAccess = (Map<String, List<Object>>) convertedClaims.get("resource_access");
        logger.debug("keySet :" + resourceAccess.keySet().toString() + ", entrySet : " + resourceAccess.entrySet().toString());


        List<String> yourAppClaims = new ArrayList<>();
        List<String> accountClaims = new ArrayList<>();
        for (Map.Entry<String, List<Object>> entry: resourceAccess.entrySet()) {
            String resourceKey = entry.getKey();
            if(resourceKey.equalsIgnoreCase("jwt")) {
                logger.info("*********  app claims received*******");
                Map<String, List<String>> resourceClaims = (Map<String, List<String>>) entry.getValue();
                for (Map.Entry<String, List<String>> childEntry : resourceClaims.entrySet()) {
                    String resourceClaimsKey = childEntry.getKey();
                    List<String> resourceClaimsValue = childEntry.getValue();
                    logger.debug(resourceClaimsKey + "=" + resourceClaimsValue.toString());
                    yourAppClaims = resourceClaimsValue;
                }
            }
        }
        //mapped roles into SimpleGrantedAuthority
       if(yourAppClaims.size() > 0){
           List<SimpleGrantedAuthority> roles = yourAppClaims.stream()
                   .map(roleName -> "ROLE_" + roleName)
                   .map(SimpleGrantedAuthority::new)
                   .collect(Collectors.toList());
           convertedClaims.put("roles",roles);
       }

       if(logger.isDebugEnabled()) {
           List<SimpleGrantedAuthority> roles = (List<SimpleGrantedAuthority>) convertedClaims.get("roles");
           for (SimpleGrantedAuthority authority : roles) {
               logger.debug("resourceAccess roles :" + authority.getAuthority().toString());
           }
       }
        return convertedClaims;
    }
}