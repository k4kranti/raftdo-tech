package sara.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
public class JoseConfig extends WebSecurityConfigurerAdapter {

    private static final Log logger = LogFactory.getLog(JoseConfig.class);

    // http config for authorize requests
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests(a -> a
                        .antMatchers("/public/**")
                        .permitAll()
                        .anyRequest()
                        .authenticated())
                //.oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);
                .oauth2ResourceServer().jwt(j -> j.jwtAuthenticationConverter(converter()));
    }

    private JwtAuthenticationConverter converter() {
        JwtAuthenticationConverter authenticationConverter = new JwtAuthenticationConverter();
        //look for scope claims or scp claims and convert to Jwt
        JwtGrantedAuthoritiesConverter authoritiesConverter = new JwtGrantedAuthoritiesConverter();
        authenticationConverter.setJwtGrantedAuthoritiesConverter(jwt -> {
            //scopes - authority user granted to client ( user profile_info to share to jose app)
            Collection<GrantedAuthority> authorities = authoritiesConverter.convert(jwt);
            logger.info("SCOPES:" + authorities.toString());
            //claims
            Map<String, Object> claims = jwt.getClaims();
            claims.forEach((k,v) -> logger.info("KEY = "+k + ", VALUE = "+v));
            if (claims.containsKey("resource_access")) {
                List<String> appClaims = new ArrayList<>();
                com.nimbusds.jose.shaded.json.JSONObject resourceAccess = (com.nimbusds.jose.shaded.json.JSONObject) claims.get("resource_access");
                if(resourceAccess.containsKey("jose")){
                    // move to custom class which extend spring core converter?
                    String appAccess = resourceAccess.getAsString("jose");
                    if(appAccess.contains("READ")){
                        appClaims.add("READ");
                    }
                    if(appAccess.contains("WRITE")){
                        appClaims.add("WRITE");
                    }
                }
                //roles - authority app granted to user (jose app to user or tester)
                List<SimpleGrantedAuthority> roles = appClaims.stream()
                        .map(roleName -> "ROLE_" + roleName)
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());
                authorities.addAll(roles);
            }
           return authorities;
        });
        return authenticationConverter;
    }

}
