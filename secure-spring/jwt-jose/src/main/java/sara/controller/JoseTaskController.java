package sara.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sara.model.JoseTask;
import sara.service.JoseTaskService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/tasks")
public class JoseTaskController {

    private static final Log logger = LogFactory.getLog(JoseTaskController.class);
    JoseTaskService taskService;

    public JoseTaskController(JoseTaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = "application/json")
    @PreAuthorize("hasAuthority('ROLE_READ')")
    public List<JoseTask> getTasks(@AuthenticationPrincipal Jwt jwt){
        String user = jwt.getClaim("preferred_username");
        if(null == user) user = jwt.getClaim("given_name");
        logger.debug("user:" + user);
        return taskService.getTasksByOwner(user);
    }

    @GetMapping(path="/{id}",produces = "application/json")
    @PreAuthorize("hasAuthority('ROLE_READ')")
    @PostAuthorize("@owner.apply(returnObject, principal.claims['preferred_username'])")
    public Optional<JoseTask> getTask(@PathVariable("id") long id){
        return this.taskService.read(id);
    }

    @PostMapping(consumes ="application/json",produces = "application/json")
    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    public JoseTask postTasks(@RequestBody String taskName, @AuthenticationPrincipal Jwt jwt){
        JoseTask task = new JoseTask();
        task.setOwner(jwt.getClaim("preferred_username"));
        task.setTask(taskName);
        return this.taskService.save(task);
    }

    @PutMapping(path="{id}/update", consumes ="application/json", produces = "application/json")
    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    @PostAuthorize("@owner.apply(returnObject, principal.claims['preferred_username'])")
    @Transactional
    public Optional<JoseTask> updateTask(@PathVariable("id") long id,@RequestBody String text){
        logger.info("id: "+ id +", text:" + text);
        this.taskService.updateTask(id,text);
        return getTask(id);
    }


    @PutMapping(path="{id}/completed", produces ="application/json")
    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    @PostAuthorize("@owner.apply(returnObject, principal.claims['preferred_username'])")
    @Transactional
    public Optional<JoseTask> completeTask(@PathVariable("id") long id){
        taskService.completeTask(id);
        return getTask(id);
    }

}
