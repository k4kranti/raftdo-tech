package sara;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class JoseTaskApplicationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getTasks_WhenNoToken_ThenUnAuthenticate() throws Exception{
        ResultMatcher unAuthorized = MockMvcResultMatchers.status().isUnauthorized();
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/tasks"))
                        //.with(SecurityMockMvcRequestPostProcessors.jwt()))
                .andExpect(unAuthorized);
    }

    @Test
    public void getTasks_WhenNoScope_ThenForbidden() throws Exception{
        ResultMatcher isForbidden = MockMvcResultMatchers.status().isForbidden();
        this.mockMvc.perform(MockMvcRequestBuilders
                       .get("/tasks")
                       .with(SecurityMockMvcRequestPostProcessors.jwt()))
                    .andExpect(isForbidden);
    }

    @Test
    public void getTasks_WhenWrongScope_ThenForbidden() throws Exception{
        ResultMatcher isForbidden = MockMvcResultMatchers.status().isForbidden();
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/tasks")
                        .with(SecurityMockMvcRequestPostProcessors
                                .jwt()
                                .authorities(new SimpleGrantedAuthority("ROLE_TEST"))))
                .andExpect(isForbidden);
    }

    @Test
    public void getTasks_WhenReadScope_ThenSuccess() throws Exception{
        ResultMatcher isOkay = MockMvcResultMatchers.status().isOk();
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/tasks")
                        .with(SecurityMockMvcRequestPostProcessors
                                .jwt()
                                .authorities(new SimpleGrantedAuthority("ROLE_READ"))))
                .andExpect(isOkay);
    }

    @Test
    public void getTask_WhenAuthUserWithReadScope_ThenSuccess() throws Exception{
        ResultMatcher isOkay = MockMvcResultMatchers.status().isOk();
        this.mockMvc.perform(MockMvcRequestBuilders.get("/tasks/1")
                        .with(SecurityMockMvcRequestPostProcessors
                                .jwt().jwt(j -> j.claim("preferred_username","user")
                                ).authorities(new SimpleGrantedAuthority("ROLE_READ"))))
                        .andExpect(isOkay);
    }

    @Test
    public void getTask_WhenUnAuthUserWithReadScope_ThenForbidden() throws Exception{
        ResultMatcher isForbidden = MockMvcResultMatchers.status().isForbidden();
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/tasks/1")
                        .with(SecurityMockMvcRequestPostProcessors
                                .jwt().jwt(j -> j.claim("preferred_username","tester")
                                ).authorities(new SimpleGrantedAuthority("ROLE_READ"))))
                .andExpect(isForbidden);
    }

}