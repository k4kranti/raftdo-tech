#!bin/bash
echo "__________________"
echo "<<< get the user token"
USER_TOKEN=$( curl -d 'client_id=opaque' -d 'username=user' -d 'password=password' -d 'grant_type=password' -d 'client_secret=IiWmzoOojzz5LfEIp2r5xt5omZGRp1Pa'  http://localhost:8080/auth/realms/Raftdo_tech/protocol/openid-connect/token | jq -r '.access_token')
echo $USER_TOKEN

echo "________user access his tasks_____________"
curl -H "Authorization: Bearer ${USER_TOKEN}" localhost:8085/tasks -v | jq

echo "--------user try update status___________"
curl -H "Authorization: Bearer ${USER_TOKEN}" -X PUT localhost:8085/tasks/1/completed -v | jq
