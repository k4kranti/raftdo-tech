package sara;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class OpaqueApplicationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getTasks_WhenNoToken_ThenUnAuthenticate() throws Exception{
        ResultMatcher unAuthorized = MockMvcResultMatchers.status().isUnauthorized();
        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/tasks"))
                        //.with(SecurityMockMvcRequestPostProcessors.opaqueToken()))
                .andExpect(unAuthorized);
    }

    @Test
    public void getTasks_WhenNoScope_ThenForbidden() throws Exception{
        ResultMatcher isForbidden = MockMvcResultMatchers.status().isForbidden();
        java.util.Map<String,Object> mockatt= new HashMap<>();
        mockatt.put("username","user");
        mockatt.put("client_id","opaque");
        mockatt.put("resource_access","{\"opaque\":{\"roles\":[\"READ\",\"WRITE\"]}");
        this.mockMvc.perform(MockMvcRequestBuilders
                       .get("/tasks")
                       .with(SecurityMockMvcRequestPostProcessors.opaqueToken()
                               .attributes((Consumer<Map<String, Object>>) mockatt)))
                    .andExpect(isForbidden);
    }


}