package sara.config;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@AuthenticationPrincipal(expression = "attributes['username']")
public @interface OpaqueUser {
}
