package sara.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class OpaqueTokenIntrospectorDelegate implements OpaqueTokenIntrospector {
    private final OpaqueTokenIntrospector delegate;
    private static final Log logger = LogFactory.getLog(OpaqueTokenIntrospectorDelegate.class);

    public OpaqueTokenIntrospectorDelegate(OpaqueTokenIntrospector delegate){
        this.delegate = delegate;
    }

    @Override
    public OAuth2AuthenticatedPrincipal introspect(String token){
        OAuth2AuthenticatedPrincipal principal = this.delegate.introspect(token);

        String name = principal.getName();
        Map<String, Object> attributes = new HashMap<>(principal.getAttributes());
        attributes.put("user",principal.getAttribute("username"));
        Collection<GrantedAuthority> authorities = new ArrayList<>(principal.getAuthorities());
        net.minidev.json.JSONObject resourceAccess = principal.getAttribute("resource_access");
        if(resourceAccess.containsKey("opaque")){
            String appAccess = resourceAccess.getAsString("opaque");
            logger.info("appAccess:" + resourceAccess);
            if(appAccess.contains("READ")){
                authorities.add(new SimpleGrantedAuthority("ROLE_READ"));
            }
            if(appAccess.contains("WRITE")){
                authorities.add(new SimpleGrantedAuthority("ROLE_WRITE"));
            }
        }
        return new DefaultOAuth2AuthenticatedPrincipal(name,attributes, authorities);

    }
}
