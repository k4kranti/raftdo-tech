package sara.service;

import org.springframework.stereotype.Service;
import sara.model.OpaqueTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OpaqueTaskService {

    List<OpaqueTask> taskList = new ArrayList<>();
    OpaqueTask task1 = new OpaqueTask(1, "user" , "PROJECT PLAN", false);
    OpaqueTask task2 = new OpaqueTask(2, "tester", "TEST ENGAGEMENT", false);

    public OpaqueTaskService(){
        taskList.add(task1);
        taskList.add(task2);
    }

    public List<OpaqueTask> getTasks(String owner){
        return taskList;
    }

    public List<OpaqueTask> getTasksByOwner(String owner){
        List<OpaqueTask> ownerTasks = new ArrayList<>();
        for (OpaqueTask task : taskList) {
            if(task.getOwner().equalsIgnoreCase(owner)){
                ownerTasks.add(task);
            }
        }
        return ownerTasks;
    }

    public Optional<OpaqueTask> read(long id){
        Optional<OpaqueTask> taskOptional = null;
        for (OpaqueTask task : taskList) {
           if(task.getId() == id){
               taskOptional = Optional.of(task);
           }
        } return taskOptional;
    }

    public OpaqueTask save(OpaqueTask task){
        taskList.add(task);
        return task;
    }

    public void updateTask(long id,String text){
        for (OpaqueTask task : taskList) {
            if(task.getId() == id){
                task.setTask(text);
            }
        }
    }

    public void completeTask(long id){
        for (OpaqueTask task : taskList) {
            if(task.getId() == id){
                task.setCompleted(true);
            }
        }
    }
}
