package sara.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sara.config.OpaqueUser;
import sara.model.OpaqueTask;
import sara.service.OpaqueTaskService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/tasks")
public class OpaqueTaskController {

    private static final Log logger = LogFactory.getLog(OpaqueTaskController.class);
    OpaqueTaskService taskService;

    public OpaqueTaskController(OpaqueTaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = "application/json")
    public List<OpaqueTask> getTasks(@AuthenticationPrincipal OAuth2AuthenticatedPrincipal principal){
        logger.info("principal:" + principal.getAttributes());
        String user = principal.getAttribute("username").toString();
        logger.info("user:" + user);
        return taskService.getTasksByOwner(user);
    }

    @GetMapping(path="/{id}",produces = "application/json")
    public Optional<OpaqueTask> getTask(@PathVariable("id") long id){
        return this.taskService.read(id);
    }

    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    @PostMapping(consumes ="application/json",produces = "application/json")
    public OpaqueTask postTasks(@RequestBody String taskName, @OpaqueUser String user){
        OpaqueTask task = new OpaqueTask();
        task.setOwner(user);
        task.setTask(taskName);
        return this.taskService.save(task);
    }

    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    @PutMapping(path="{id}/update", consumes ="application/json", produces = "application/json")
    @Transactional
    public Optional<OpaqueTask> updateTask(@PathVariable("id") long id,@RequestBody String text){
        logger.info("id: "+ id +", text:" + text);
        this.taskService.updateTask(id,text);
        return getTask(id);
    }


    @PreAuthorize("hasAuthority('ROLE_WRITE')")
    @PutMapping(path="{id}/completed", produces ="application/json")
    @Transactional

    public Optional<OpaqueTask> completeTask(@PathVariable("id") long id){
        taskService.completeTask(id);
        return getTask(id);
    }

}
