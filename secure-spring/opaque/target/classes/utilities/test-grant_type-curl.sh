#!bin/bash
echo "__________________"
#To get discovery document
curl 'http://localhost:8080/auth/realms/master/.well-known/openid-configuration' | jq "."

echo ">>>> grant_type=password (back channel only)"
echo "get the token via grant_type=password "
TOKEN=$( curl -d 'client_id=jose' -d 'username=user' -d 'password=password' -d 'grant_type=password' -d 'client_secret=0xr6rXUm6Sr8jxkGBsmxKR7mmAQQcA70'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq -r '.access_token')
echo $TOKEN
echo "________check the userinfo (OpenId connect feature, /userinfo,Standard set of scopes_________________"
curl -H "Authorization: Bearer ${TOKEN}" http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/userinfo | jq "."
echo "________access your app tasks_________________"
curl  'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" localhost:8082/tasks -v | jq "."

echo ">>>> grant_type=authorization_code(fontend + backend channels)"
# usecase : Web server apps and not available to public access directly
#Server apps (auth token stores as cookie in your app) response_type=code
curl --silent --output /dev/null --cookie-jar - http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/auth?response_type=code&client_id=jose&client_secret=0xr6rXUm6Sr8jxkGBsmxKR7mmAQQcA70&state=raftdo1234&scope=profile&redirect_uri=http://localhost:8082/tasks | grep KC_RESTART | cut -f7- | pbcopy
# validate the token https://jwt.io/
pbpaste
#callback authorization code (your edge service should have this redirect logic)
#curl http://localhost:8082/callback?state=raftdo1234&code=ciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJlMjRjNTQ4Mi01NmE0LTRkNTYtYjgyNC0yZjFjODdmM2M4ZmIifQ.eyJjaWQiOiJqb3NlIiwicHR5Ijoib3BlbmlkLWNvbm5lY3QiLCJydXJpIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgyL3Rhc2tzIiwiYWN0IjoiQVVUSEVOVElDQVRFIiwibm90ZXMiOnsiY2xpZW50X3JlcXVlc3RfcGFyYW1fY2xpZW50X3NlY3JldCI6IjB4cjZyWFVtNlNyOGp4a0dCc214S1I3bW1BUVFjQTcwIiwic2NvcGUiOiJwcm9maWxlIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL3JhZnRkbyIsInJlc3BvbnNlX3R5cGUiOiJjb2RlIiwicmVkaXJlY3RfdXJpIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgyL3Rhc2tzIiwic3RhdGUiOiJyYWZ0ZG8xMjM0In19.1mLlKvYimZdrYon0qe-K1nDIitzd4Z-rPad4kk85z4w
#exchange code for access token
curl -X POST http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'client_id=jose' \
--data-urlencode 'username=user' \
--data-urlencode 'client_secret=0xr6rXUm6Sr8jxkGBsmxKR7mmAQQcA70' \
--data-urlencode 'code=ciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJlMjRjNTQ4Mi01NmE0LTRkNTYtYjgyNC0yZjFjODdmM2M4ZmIifQ.eyJjaWQiOiJqb3NlIiwicHR5Ijoib3BlbmlkLWNvbm5lY3QiLCJydXJpIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgyL3Rhc2tzIiwiYWN0IjoiQVVUSEVOVElDQVRFIiwibm90ZXMiOnsiY2xpZW50X3JlcXVlc3RfcGFyYW1fY2xpZW50X3NlY3JldCI6IjB4cjZyWFVtNlNyOGp4a0dCc214S1I3bW1BUVFjQTcwIiwic2NvcGUiOiJwcm9maWxlIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL3JhZnRkbyIsInJlc3BvbnNlX3R5cGUiOiJjb2RlIiwicmVkaXJlY3RfdXJpIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgyL3Rhc2tzIiwic3RhdGUiOiJyYWZ0ZG8xMjM0In19.1mLlKvYimZdrYon0qe-K1nDIitzd4Z-rPad4kk85z4w' \
--data-urlencode 'redirect_uri=http://localhost:8082/'

echo ">>>> grant_type=client_credentials (back channel only)"
# usecase : application access without a user present,access token to act on behalf of
#curl -d 'grant_type=client_credentials&client_id=jose&client_secret=0xr6rXUm6Sr8jxkGBsmxKR7mmAQQcA70'  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token | jq -r '.access_token' | pbcopy
curl  http://localhost:8080/auth/realms/raftdo/protocol/openid-connect/token \
-d 'grant_type=client_credentials' \
-d 'client_id=jose' \
-d 'client_secret=0xr6rXUm6Sr8jxkGBsmxKR7mmAQQcA70' | jq -r '.access_token' | pbcopy
# validate the token https://jwt.io/


echo ">>>> grant_type=authorization_code with pkce (front + back channels like mobile apps)"