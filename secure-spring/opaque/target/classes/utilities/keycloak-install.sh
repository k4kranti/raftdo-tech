#!bin/bash
echo "docker start.."
docker start CONTAINER raftdo
echo "docker keycloak image.."
docker run -p 8080:8080 --name authserver -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e DB_VENDOR=h2 quay.io/keycloak/keycloak:16.1.1
#Database info: {databaseUrl=jdbc:h2:/opt/jboss/keycloak/standalone/data/keycloak, databaseUser=SA, databaseProduct=H2 1.4.197 (2018-03-18), databaseDriver=H2 JDBC Driver 1.4.197 (2018-03-18)