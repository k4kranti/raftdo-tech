#!/bin/bash

if [ $# -ne 5 ]; then
  echo 1>&2 "Usage: . $0 hostname realm username client_id"
  echo 1>&2 "  options:"
  echo 1>&2 "    hostname: localhost:8080"
  echo 1>&2 "    realm:raftdo"
  echo 1>&2 "    client_id:jwt"
  echo 1>&2 "    For verify ssl: use 'y' (otherwise it will send curl post with --insecure)"
  return
fi

HOSTNAME=$1
REALM_NAME=$2
USERNAME=$3
CLIENT_ID=$4
SECURE=$5

KEYCLOAK_URL=http://$HOSTNAME:8080/auth/realms/$REALM_NAME/protocol/openid-connect/token

echo "KEYCLOAK TOKEN URL: $KEYCLOAK_URL"
echo "REALM:              $REALM_NAME"
echo "CLIENT_ID:         $CLIENT_ID"
echo "USERNAME:           $USERNAME"
echo "SECURE:             $SECURE"


if [[ $SECURE = 'y' ]]; then
	INSECURE=
else
	INSECURE=--insecure
fi

echo -n Password:
read -s PASSWORD

export TOKEN=$(curl -X POST "$KEYCLOAK_URL" "$INSECURE" \
 -H "Content-Type: application/x-www-form-urlencoded" \
 -d "username=$USERNAME" \
 -d "password=$PASSWORD" \
 -d 'grant_type=password' \
 -d "client_id=$CLIENT_ID" \
 -d "client_secret=Pc0zp0b7KCvZDuIlP1VAJOSYJ7jy64sW"| jq -r '.access_token')

echo $TOKEN
export JWT=$TOKEN

if [[ $(echo $TOKEN) != 'null' ]]; then
	export JWT=$TOKEN
fi