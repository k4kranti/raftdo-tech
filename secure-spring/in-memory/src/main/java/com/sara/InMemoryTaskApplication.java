package com.sara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InMemoryTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(InMemoryTaskApplication.class, args);
    }
}


