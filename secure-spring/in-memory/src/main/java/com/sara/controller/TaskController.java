package com.sara.controller;

import com.sara.model.Task;
import com.sara.service.TaskService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/tasks")
public class TaskController {

    private final TaskService taskService;
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(produces = "application/json")
    public List<Task> getTasks(){
        return taskService.getTasks();
    }

    @PostMapping(consumes ="application/json")
    public void postTasks(@RequestBody Task task){
        taskService.postTasks(task);
    }
}
