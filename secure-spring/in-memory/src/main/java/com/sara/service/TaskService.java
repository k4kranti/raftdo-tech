package com.sara.service;

import com.sara.model.Task;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {

    List<Task> taskList = new ArrayList<>();
    Task task1 = new Task(1, "STEPHEN" , "PROJECT PLAN", true);
    Task task2 = new Task(2, "SARA LEE", "TEAM ENGAGEMENT", false);

    public TaskService(){
        taskList.add(task1);
        taskList.add(task2);
    }
    public List<Task> getTaskList() {
        return taskList;
    }

    public List<Task> getTasks(){
        return getTaskList();
    }

    public void postTasks(Task task){
        getTaskList().add(task);
    }
}
