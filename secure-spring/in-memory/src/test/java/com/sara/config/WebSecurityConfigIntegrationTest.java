package com.sara.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WebSecurityConfigIntegrationTest {

    @LocalServerPort
    int port;

    @Test
    public void whenLoggedUser_ThenSuccess()
            throws IllegalStateException, IOException {
        TestRestTemplate restTemplate = new TestRestTemplate("user", "password");
        URL base = new URL("http://localhost:" + port + "/tasks");
        ResponseEntity<String> response =
                restTemplate.getForEntity(base.toString(), String.class);
        //Verify request succeed
        Assertions.assertEquals(HttpStatus.OK,  response.getStatusCode());
        Assertions.assertTrue(Objects.requireNonNull(response.getBody()).contains("STEPHEN"));
    }

    @Test
    public void whenUserWithWrongCredentials_thenUnauthorized() throws Exception {
        TestRestTemplate restTemplate;
        URL base = new URL("http://localhost:" + port+"/tasks");
        restTemplate = new TestRestTemplate("user", "wrongpassword");
        ResponseEntity<String> response =
                restTemplate.getForEntity(base.toString(), String.class);
        Assertions.assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }
}