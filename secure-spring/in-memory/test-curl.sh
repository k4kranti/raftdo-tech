#!bin/bash
echo "unauthorized tasks:"
curl localhost:9080/tasks -v | jq '.error'
echo "authorized tasks:"
curl -u user:password localhost:9080/tasks -v | jq
echo "Add authorized user tasks"
curl -u user:password -d '{"id": 10,"owner": "ROBERT","task": "QA","completed": false}' -H "Content-Type: application/json" -X POST http://localhost:8080/tasks | jq
curl -u user:password -d '{"id": 11,"owner": "HELEN","task": "AUTOMATION","completed": false}' -H "Content-Type: application/json" -X POST http://localhost:8080/tasks | jq
echo "authorized tasks:"
curl -u user:password localhost:9080/tasks -v | jq